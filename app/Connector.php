<?php

class Connector
{
    /**
     * @var AMQPConnection
     */
    private $connection;
    private $timelimit;

    /**
     * Connector constructor.
     */
    public function __construct(AMQPConnection &$connection, $timelimit)
    {

        $this->connection = $connection;
        $this->timelimit  = $timelimit;
    }

    /**
     * @throws AMQPConnectionException
     */
    public function connect() {
        $err = new \Exception();
        while ($this->timelimit > 0) {
            try {
               return $this->connection->connect();
            } catch (AMQPConnectionException $e) {
                sleep(1);
                $this->timelimit--;
                $err = $e;
            }
        }
        throw $err;
    }
}
