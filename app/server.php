<?php

require_once __DIR__ . "/Connector.php";
$config = [
    "host" => "rabbitmq",
    "port" => "5672",
    "login" => "guest",
    "password" => "guest",
];

$connection = new AMQPConnection($config);
$connector  = new Connector($connection, 50);
$connector->connect();
$chanel     = new AMQPChannel($connection);
$exchange = new AMQPExchange($chanel);
$exchange->setName('simple_exchange');
$exchange->setType(AMQP_EX_TYPE_TOPIC);
$exchange->declareExchange();
$que = new AMQPQueue($chanel);
$que->setName("my_queue");
$que->setFlags(AMQP_DURABLE);
$que->declareQueue();
$que->bind("simple_exchange", "routing_key");

$callBack = function ($envelope, $queue) {
    var_dump("Hello world");
};

$que->consume($callBack, AMQP_AUTOACK);
