IMG=php-amqp
TAG=latest
$(IMG):
	@docker build -t $(IMG):$(TAG) .

up:
	@docker-compose up

.DEFAULT_GOAL := $(IMG)