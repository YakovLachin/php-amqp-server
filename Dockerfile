FROM php:7.0-cli-alpine
RUN apk update && \
    apk add make cmake gcc alpine-sdk git openssl-dev autoconf libtool && \
    git clone https://github.com/alanxz/rabbitmq-c.git && \
    cd rabbitmq-c && \
    git checkout v0.8.0 && \
    mkdir  build && cd build && \
    cmake -DOPENSSL_ROOT_DIR=/etc/ssl -DOPENSSL_LIBRARIES=/usr/lib -DCMAKE_INSTALL_PREFIX=/usr/local .. && \
    cmake build . && \
    make install && \
    cd /usr/src && \
    wget http://pecl.php.net/get/amqp-1.7.0.tgz && tar xvfz amqp-1.7.0.tgz && cd amqp-1.7.0 && \
    phpize && ./configure --with-amqp && \
    make && \
    mkdir -p /usr/local/lib/php/extensions/no-debug-non-zts-20151012 && \
    cp /usr/src/amqp-1.7.0/modules/amqp.so /usr/local/lib/php/extensions/no-debug-non-zts-20151012 && \
    cp /rabbitmq-c/build/librabbitmq/librabbitmq.so.4 /usr/local/lib


COPY ./ /app
COPY ./config/php.ini /usr/local/etc/php

CMD ["php", "/app/server.php"]
